from __future__ import absolute_import, unicode_literals


UFO2FT_PREFIX = 'com.github.googlei18n.ufo2ft.'
GLYPHS_PREFIX = 'com.schriftgestaltung.'

USE_PRODUCTION_NAMES = UFO2FT_PREFIX + "useProductionNames"
GLYPHS_DONT_USE_PRODUCTION_NAMES = GLYPHS_PREFIX + "Don't use Production Names"
